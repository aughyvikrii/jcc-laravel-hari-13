@extends('master')

@section('judul', 'Detail Cast')

@section('content')
<h2>Cast: {{$cast->id}}</h2>
<p>
    <ul>
        <li><b>Nama:</b> {{$cast->nama}}</li>
        <li><b>Umur:</b> {{$cast->umur}}</li>
        <li><b>Bio:</b> {{$cast->bio}}</li>
    </ul>

    <div class="d-flex">
        <a href="/cast/{{$cast->id}}/edit">
            <button type="button" class="btn btn-sm btn-warning">Edit</button>
        </a> &nbsp;
    
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method("DELETE")
            <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
        </form>
    </div>
</p>
@endsection