@extends('master')

@section('judul', 'Daftar Cast')

@section('content')
<table class="table table-bordered">
    <thead>
        <tr>
            <th style="width: 10px;">ID</th>
            <th>Nama</th>
            <th>Umur</th>
            <th style="width: 40px;">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach($casts as $cast)
        <tr>
            <td>{{ $cast->id }}</td>
            <td>{{ $cast->nama }}</td>
            <td>{{ $cast->umur }}</td>
            <td class="d-flex">
                <a href="/cast/{{$cast->id}}">
                    <button type="button" class="btn btn-sm btn-info">Detail</button>
                </a> &nbsp;
                <a href="/cast/{{$cast->id}}/edit">
                    <button type="button" class="btn btn-sm btn-warning">Edit</button>
                </a> &nbsp;
                <form action="/cast/{{$cast->id}}" method="POST">
                    @csrf
                    @method("DELETE")
                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection